cmake_minimum_required(VERSION 3.9.6...3.19)

project(
	Sequencer
	VERSION 0.1
	LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(SRC_FILES
	"${Sequencer_SOURCE_DIR}/src/clock.cpp"
	"${Sequencer_SOURCE_DIR}/src/sequence.cpp"
	"${Sequencer_SOURCE_DIR}/src/sequencer.cpp"
	"${Sequencer_SOURCE_DIR}/src/command_queue.cpp")

set(HDR_FILES
	"${Sequencer_SOURCE_DIR}/src/clock.h"
	"${Sequencer_SOURCE_DIR}/src/sequence.h"
	"${Sequencer_SOURCE_DIR}/src/sequencer.h"
	"${Sequencer_SOURCE_DIR}/src/command_queue.h")

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(ELEMENTS_ROOT "extern/elements")
add_subdirectory(src)
add_subdirectory(tests)
