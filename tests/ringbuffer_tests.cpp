#include <catch2/catch.hpp>

#include "../src/ring_buffer.h"

TEST_CASE("Add and remove elements", "[RingBuffer]")
{
	const std::size_t N{8};
	RingBuffer<int, N> rb{};
	SECTION("Initialises as empty") {
		REQUIRE(rb.count() == 0);
	}
	rb.push(42);
	rb.push(24);
	SECTION("Pushing increases count and updates front") {
		REQUIRE(rb.count() == 2);
		REQUIRE(rb.front() == 42);
	}
	SECTION("Removing decreases count and updates front") {
		rb.pop();
		REQUIRE(rb.count() == 1);
		REQUIRE(rb.front() == 24);
	}
	SECTION("Pushing to a full buffer raises exception") {
		bool exception_raised{false};
		while (rb.count() < N)
			rb.push(97);
		try {
			rb.push(99);
		}
		catch(std::exception &e) {
			exception_raised = true;
		}
		REQUIRE(exception_raised);
	}	
	SECTION("Buffer wraps around") {
		for (int i{0}; i < 2*N; ++i) {
			rb.push(i);
			rb.pop();
		};
		REQUIRE(rb.count() == 2);
		REQUIRE(rb.front() == 2*N - 2);
	}	
}

