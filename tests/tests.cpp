#include <catch2/catch.hpp>

#include "../src/clock.h"
#include "../src/types.h"
#include "../src/sequence.h"
#include "../src/sequencer.h"

#include <memory>
#include <vector>

#include <iostream>

TEST_CASE("Returns the correct sample offsets", "[Clock]")
{
	n_frames_t sample_rate{48000};
	double tempo{123};
	int ticks_per_beat{24};
	// => samples per tick = 975.6097561
	double samples_per_tick = 60 * sample_rate / (tempo * ticks_per_beat);
	Clock clock(tempo, sample_rate, ticks_per_beat);
	SECTION("First tick is correct.") {
		for (int i{0}; i < 10; ++i)
			REQUIRE(clock.SamplesToNextTick() == 
					static_cast<n_frames_t>(samples_per_tick));
	}
	SECTION("Advancing zero frames doesn't do anything (apart from initially)") {
		clock.Advance(static_cast<n_frames_t>(samples_per_tick) - 10);
		for (int i{0}; i < 10; ++i) {
			clock.Advance(0);
			REQUIRE(clock.SamplesToNextTick() == 10);
		}
	}
	SECTION("Advancing part way through a tick reduces the samples to the next tick") {
		clock.Advance(static_cast<n_frames_t>(samples_per_tick) - 10);
		REQUIRE(clock.SamplesToNextTick() == 10);
	}
	SECTION("Advancing to the next sample makes SamplesToNextTick refer to"
			"the sample after") {
		n_frames_t expected;
		for (int i{1}; i < 1000; ++i) {
			clock.Advance(clock.SamplesToNextTick());
			expected = static_cast<n_frames_t>((i + 1) * samples_per_tick) - 
				static_cast<n_frames_t>(i * samples_per_tick);
			REQUIRE(clock.SamplesToNextTick() == expected);
		}
	}
	SECTION("Samples adjusted correctly whnn advancing multiple ticks") {
		clock.Advance(100000);
		REQUIRE(clock.SamplesToNextTick() == 487);
	}
	SECTION("Advancing several times is the same as once") {
		for (int i{0}; i < 10; ++i)
			clock.Advance(10000);
		REQUIRE(clock.SamplesToNextTick() == 487);
	}
	/*
	std::vector<n_frames_t>  sample_offsets_1{};
	for (int i = 0; i < 5; ++i)
	{
		auto sample_offsets{clock.TicksInNextNFrames(1024)};
		std::copy(sample_offsets.begin(), sample_offsets.end(),
				std::back_inserter(sample_offsets_1));
	}
	std::vector<n_frames_t> expected_1{ 975, 927, 878, 830, 782 };
	auto first_difference{std::mismatch(sample_offsets_1.begin(), sample_offsets_1.end(),
			expected_1.begin(), expected_1.end())};
	REQUIRE((first_difference.first == sample_offsets_1.end() && 
			first_difference.second == expected_1.end()));
	*/
}

bool operator==(const MidiNote& first, const MidiNote& second)
{
	if (first.tick_offset != second.tick_offset)
		return false;
	// compare message type and velocity
	for (int i{0}; i < 2; ++i) {
		if (first.data[i] != second.data[i])
			return false;
	}
	// if message is off then velocity and whether it plays doesn't need to match
	if ((first.data[0] & 0xF0) != 0x80 && 
			(first.data[2] != second.data[2] || first.play != second.play))
		return false;

	return true;
}

TEST_CASE("Gets correct next event", "[StepSequence]")
{
	MidiNote a{{0x82, 60, 65}, 3};
	MidiNote b{{0x82, 60, 0}, 3};
	REQUIRE( a == b );
	std::vector<midi_data_t> notes{ 60, 63, 67 };
	std::vector<midi_data_t> velocities{ 70, 60, 61 };
	int ticks_per_step{24};
	int note_duration{18};
	StepSequence sequence{ticks_per_step, note_duration, notes, velocities};
	std::vector<MidiNote> expected_midi_notes;
	for (int i{0}; i < 2 * notes.size(); ++i) {
		midi_data_t event_type = i % 2 == 0 ? 0x90 : 0x80;
		int offset = i % 2 == 0 ? ticks_per_step - note_duration : note_duration;
		expected_midi_notes.push_back({{event_type, notes[i/2], velocities[i/2]}, offset});
	}

	auto CheckNotes{ [&] (int events_to_check=100){
		// first call has an offset of 1
		sequence.GetNextEvent();
		for (int i{1}; i < events_to_check; ++i) {
			const MidiNote& next_event{sequence.GetNextEvent()};
			REQUIRE( next_event == expected_midi_notes[i % expected_midi_notes.size()] );
		}	
	}};

	SECTION("First call to GetNextEvent() has an offset of 1") {
		MidiNote next_event{ sequence.GetNextEvent() };
		MidiNote expected_first{expected_midi_notes.front()};
		expected_first.tick_offset = 1;
		REQUIRE( next_event == expected_first );
	}
	
	SECTION("Loops") {
		CheckNotes();
	}

	SECTION("Notes can be changed") {
		sequence.SetNote(1, 64);
		expected_midi_notes[2].data[1] = 64;
		expected_midi_notes[3].data[1] = 64;
		sequence.SetVelocity(2, 25);
		expected_midi_notes[4].data[2] = 25;
		CheckNotes();
	}
	SECTION("Skips") {
		sequence.SetPlayNote(1, false);
		auto original_expected{expected_midi_notes};
		// when a note is set to not play, we won't get the note off message
		expected_midi_notes.erase(expected_midi_notes.begin() + 3);
		expected_midi_notes[2].play = false;
		expected_midi_notes[3].tick_offset = ticks_per_step;
		CheckNotes();
		sequence.SetPlayNote(1, true);
		sequence.Reset();
		expected_midi_notes = original_expected;
		CheckNotes();
	}
}

TEST_CASE("Sequences can be added and removed from sequencer", "[Sequencer]")
{
	auto sequence{std::make_unique<StepSequence>(24, 18, 
			std::vector<midi_data_t>{ 60, 63, 67 },
			std::vector<midi_data_t>{ 70, 60, 61 })};
	auto expected = sequence.get();
	Sequencer sequencer{120, 4, 48000};
	size_t id{ sequencer.AddSequence(std::move(sequence)) };
	sequence = nullptr;
	SECTION("The sequence can be retrieved with the id returned when adding") {
		auto& sequence_ref = sequencer.GetSequence(id);
		REQUIRE(&sequence_ref == expected);
	}
	SECTION("Exception when getting a removed sequence") {
		sequencer.RemoveSequence(id);
		bool exception_raised{false};
		try {
			sequencer.GetSequence(id);
		}
		catch(const std::exception& e) {
			exception_raised = true;
		}
		REQUIRE(exception_raised == true);
	}	
}

TEST_CASE("Sequencer processes the correct midi events", "[Sequencer]")
{
	auto sequence1{std::make_unique<StepSequence>(
			StepSequence::BEAT,
			StepSequence::BEAT * 3 / 4, 
			std::vector<midi_data_t>{ 60, 63, 67 },
			std::vector<midi_data_t>{ 70, 60, 61 })};
	auto sequence2{std::make_unique<StepSequence>(
			StepSequence::HALF_BEAT,	
			StepSequence::HALF_BEAT * 3 / 4, 
			std::vector<midi_data_t>{ 67, 70, 74, 70 },
			std::vector<midi_data_t>{ 64, 64, 64, 64 })};
	// 
	double tempo{123};
	n_frames_t sample_rate{48000};
	int bar_length{4};
	Sequencer sequencer{tempo, bar_length, sample_rate};
	int event_count{0};
	std::vector<midi_data_t> output{};
	double frames_per_beat{ 60 * sample_rate / tempo };
	std::vector<n_frames_t> absolute_event_frames{
		static_cast<n_frames_t>(1 * frames_per_beat / 24) - 1,
		static_cast<n_frames_t>(1 * frames_per_beat / 24) - 1,
		static_cast<n_frames_t>(10 * frames_per_beat / 24) - 1,
		static_cast<n_frames_t>(13 * frames_per_beat / 24) - 1,
		static_cast<n_frames_t>(19 * frames_per_beat / 24) - 1,
		static_cast<n_frames_t>(22 * frames_per_beat / 24) -1
	};

	n_frames_t sample_size{975};

	sequencer.SetWriteMidiCallback(
		[&](midi_data_t* data, size_t size, n_frames_t offset) {
			REQUIRE(offset <= sample_size);
			switch (event_count) {
				case 0: // both first notes on
					REQUIRE(size == 3);
					REQUIRE(data[0] == 0x90);
					REQUIRE(data[1] == 60);
					REQUIRE(data[2] == 70);
					break;
				case 1:
					REQUIRE(size == 3);
					REQUIRE(data[0] == 0x90);
					REQUIRE(data[1] == 67);
					REQUIRE(data[2] == 64);
					break;
				case 2: // first note of sequence 2 off
					REQUIRE(size <= 3);
					REQUIRE(data[0] == 0x80);
					REQUIRE(data[1] == 67);
					break;
				case 3: // first note of sequence 2 on
					REQUIRE(size == 3);
					REQUIRE(data[0] == 0x90);
					REQUIRE(data[1] == 70);
					REQUIRE(data[2] == 64);
					break;
				case 4: // first note of sequence 1 off
					REQUIRE(size <= 3);
					REQUIRE(data[0] == 0x80);
					REQUIRE(data[1] == 60);
					break;
				case 5: // second note of sequence 2 off
					REQUIRE(size <= 3);
					REQUIRE(data[0] == 0x80);
					REQUIRE(data[1] == 70);
					break;
			}
			//REQUIRE(offset == absolute_event_frames[event_count] % sample_size);
			++event_count;
	});

	std::vector<size_t> sequence_ids;
	sequence_ids.push_back(sequencer.AddSequence(std::move(sequence1)));
	sequence_ids.push_back(sequencer.AddSequence(std::move(sequence2)));
	for (auto id : sequence_ids)
		sequencer.SetSequencePlaying(id, true);
	sequencer.Play();
	for (n_frames_t processed_frames{0}; 
			processed_frames < frames_per_beat - 1; 
			processed_frames += sample_size) {
		sequencer.ProcessNFrames(sample_size);
	}
	REQUIRE(event_count == 6);
}
