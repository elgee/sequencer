/* command_queue.h
 * The command queue is used to send commands to the sequencer so that it can 
 * choose when to execute these commands. This means  we don't need to use 
 * locks to manage the sequencer which could invert the priority of the audio
 * processing thread. Only one thread can push commands onto the queue and one
 * thread reads from the queue (the processing thread). Also, objects can be 
 * allocated off of the processing thread and passed to the sequencer via commands. 
 * 
 * The sequencer can send messages to the gui using updates which are just an enum
 * and an optional int which can specify which object the update applies to.
 */

#include <chrono>
#include <memory>
#include <optional>

#include "ring_buffer.h"
#include "sequencer.h"

const size_t COMMAND_QUEUE_SIZE = 16;
const size_t UPDATE_QUEUE_SIZE = 16;

struct Update
{
	enum Type {
	};
	Type type;
	int id;
};

class Command
{
public:
	virtual std::optional<Update> execute(Sequencer& sequencer) const = 0;
	virtual ~Command(){};
};

class NullCommand final : public Command 
{
public: 
	virtual std::optional<Update> execute(Sequencer& sequencer) const { return {}; }
};

class UpdateQueue
{
public:
	void Push(Update update);
	Update GetNextUpdate();
private:
	RingBuffer<Update, UPDATE_QUEUE_SIZE> m_queue;
};

class CommandQueue
{
public:
	void Push(std::unique_ptr<Command>&& command);
	std::optional<Update> DoNextCommand(Sequencer &sequencer);
	void SetWaitDuration(const std::chrono::milliseconds &duration);
private:
	RingBuffer<std::unique_ptr<const Command>, COMMAND_QUEUE_SIZE> m_queue;
	std::chrono::milliseconds m_wait_duration;
	const NullCommand m_null_command{};
};

class PlaybackCommand: public Command
{
public:
	enum PlaybackArg {
		PLAY,
		PAUSE,
		STOP,
	};
	std::optional<Update> execute(Sequencer& sequencer) const override;
	PlaybackCommand(PlaybackArg arg);
private:
	PlaybackArg m_arg;
};

/*
class AddSequenceCommand : public Command
{
public:
	std::optional<Update> execute(Sequencer& sequencer) const override;
	AddSequenceCommand(std::unique_ptr<StepSequence>&& sequence);
private:
	std::unique_ptr<StepSequence> m_sequence;
};
*/
class SetSequencePlayingCommand : public Command
{
public:
	std::optional<Update>  execute(Sequencer& sequencer) const override;
	SetSequencePlayingCommand(int sequence_id, bool playing);
private:
	int m_sequence_id;
	bool m_playing;
};

class SetStepParameterCommand : public Command
{
public:
	enum StepParam {
		NOTE,
		VELOCITY,
		PLAY
	};

	std::optional<Update> execute(Sequencer& sequencer) const override;
	SetStepParameterCommand(int sequence_id, int step,
			StepParam param, char value);
private:
	int m_sequence_id;
	int m_step;
	StepParam m_param;
	char m_value;
};
