#include <elements.hpp>
#include <jack/jack.h>
#include <jack/midiport.h>

#include <iostream>
#include <memory>

#include "sequence.h"
#include "sequencer.h"
#include "command_queue.h"

using namespace cycfi::elements;


// jack stuff
jack_client_t *client;
jack_port_t *output_port;
void* port_buf;
CommandQueue command_queue;

int process_wrapper(jack_nframes_t frames, void* arg)
{
	auto p_sequencer{(Sequencer*)(arg)};
	port_buf = jack_port_get_buffer(output_port, frames);
	jack_midi_clear_buffer(port_buf);	
	command_queue.DoNextCommand(*p_sequencer);
	p_sequencer->ProcessNFrames(frames);
	return 0;
}

void write_midi(midi_data_t* data, size_t size, n_frames_t offset)
{
	if (jack_midi_event_write(port_buf, offset, data, size)) {
		throw std::out_of_range("Not enough space in buffer");
	}
}

// GUI
auto make_step(CommandQueue &command_queue, int sequence_id, int step_number)
{
	auto normal{
		fixed_size(
			{20, 60},
			rbox(colors::light_sky_blue, 3)
		)
	};
	auto pushed{
		fixed_size(
			{20, 60},
			rbox(colors::light_sky_blue.level(0.8), 3)
		)
	};
	auto step{share(basic_toggle_button(normal, pushed))};
	step->on_click = 
		[step, &command_queue, sequence_id, step_number](bool) mutable
		{
			command_queue.Push(std::make_unique<SetStepParameterCommand>(
						sequence_id, 
						step_number, 
						SetStepParameterCommand::PLAY,
						!step->value()));
		};
	return margin({2, 2, 2, 2},	hold(step));
}

auto make_sequence(CommandQueue &command_queue, int sequence_id, int step_count)
{
	htile_composite seq_tile;
	auto play_pause{ share(toggle_icon_button(icons::stop, icons::play, 1.2)) };
	play_pause->on_click = 
		[play_pause, &command_queue, sequence_id](bool) mutable
		{
			command_queue.Push(std::make_unique<SetSequencePlayingCommand>(
				sequence_id, !play_pause->value()));
		};
	seq_tile.push_back(play_pause);
	for (int i{0}; i < step_count; ++i)
		seq_tile.push_back(share(make_step(command_queue, sequence_id, i)));
	
	return margin({20, 20, 20, 20}, seq_tile);
}

auto make_transport_controls(view& view_, CommandQueue &command_queue)
{
	auto play_pause{ share(toggle_icon_button(icons::play, icons::pause, 1.2)) };
	play_pause->on_click = 
		[play_pause, &command_queue](bool) mutable
		{
			command_queue.Push(std::make_unique<PlaybackCommand>(
				play_pause->value() ? PlaybackCommand::PLAY : PlaybackCommand::PAUSE));
		};
	auto stop{ share(icon_button(icons::stop, 1.2)) };
	stop->on_click = 
		[play_pause, &command_queue, &view_](bool) mutable
		{
			command_queue.Push(std::make_unique<PlaybackCommand>(PlaybackCommand::STOP));
			play_pause->value(false);
			view_.refresh(*play_pause);
		};

	return margin(
		{20, 20, 20, 20},
		align_center(
			fixed_size(
				{40, 40},
				htile(
					hold(play_pause),
					hold(stop)
				)
			)
		)
	);
}

int main(int argc, char* argv[])
{
	jack_status_t jack_status; 
	client = jack_client_open("seq", JackNullOption, &jack_status);
	if (!client) {
		std::cout << "jack client couldn't open\n";
		return 0;
	}

	output_port = jack_port_register(client, "midi out", 
			JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);

	jack_nframes_t sample_rate{ jack_get_sample_rate(client)};
	double bpm{125};
	int bar_length{4};
	Sequencer sequencer{bpm, bar_length, sample_rate};
	auto sequence_1{std::make_unique<StepSequence>(
		StepSequence::HALF_BEAT, 
		static_cast<size_t>(StepSequence::HALF_BEAT * 0.9),
		std::vector<midi_data_t>{ 60, 63, 67, 63, 60, 63, 60, 67 },
		std::vector<midi_data_t>{ 64, 64, 64, 64, 64, 64, 64, 64 })};
	auto sequence_2{std::make_unique<StepSequence>(
		StepSequence::BEAT,
		static_cast<size_t>(StepSequence::BEAT * 0.5),
		std::vector<midi_data_t>{ 36, 36, 36 },
		std::vector<midi_data_t>{ 64, 64, 64 })};

	std::vector<size_t> sequence_ids;
	sequence_ids.push_back(sequencer.AddSequence(std::move(sequence_1)));
	sequence_ids.push_back(sequencer.AddSequence(std::move(sequence_2)));
	for (auto id : sequence_ids)
		sequencer.GetSequence(id).SetPlaying(true);

	sequencer.SetWriteMidiCallback(write_midi);
	jack_set_process_callback(client, process_wrapper, (void*)&sequencer);

	if (jack_activate(client)) {
		std::cout << "jack client couldn't activate\n";
		return 0;
	}

	// app 
	app _app(argc, argv, "Sequencer", "com.sequencer");
	window _win(_app.name());
	_win.on_close = [&_app]() { _app.stop(); };

	auto background = rbox(colors::linen, 0);
	view view_(_win);

	view_.content(
		vtile(
			make_sequence(command_queue, 0, 8),
			make_sequence(command_queue, 1, 3),
			make_transport_controls(view_, command_queue)
		),
		background
	);

	_app.run();
	return 0;
}

