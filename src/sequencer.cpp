#include "sequencer.h"
#include "types.h"

#include <stdexcept>
#include <algorithm>

Sequencer::Sequencer(const double bpm, const int beats_per_bar, 
		const n_frames_t sample_rate)
	: m_clock{bpm, sample_rate, ticks_per_beat}
	, m_ticks_into_bar{0}
	, m_ticks_per_bar{ticks_per_beat * beats_per_bar}
	, m_state{SequencerState::STOPPED}
	, m_active_sequences{0}
	, m_next_events{}
	, m_end_event{}
	, m_next_event{&m_end_event}
	, m_send_note_offs{false}
{
	m_start_playing.fill(0);
}

void Sequencer::ProcessNFrames(const n_frames_t samples)
{
	if (m_send_note_offs) {
		for (auto& event : m_next_events) {
			if (event.note.data[0] == 0x80)
				m_write_midi_callback(
						event.note.data.data(), 
						event.note.data.size(),
						0);
		}
		m_send_note_offs = false;
	}
	switch (m_state) {
		case SequencerState::PAUSED:
		case SequencerState::STOPPED:
			return;
		case SequencerState::PLAYING:
			UpdateAndWriteMidiEvents(samples);
			return;
	}
}
void Sequencer::UpdateAndWriteMidiEvents(const n_frames_t samples)
{
	// clock's initial position is the frame before the first frame to process
	n_frames_t samples_to_next_tick{m_clock.SamplesToNextTick()};
	n_frames_t processed_frames{0};
	while (processed_frames + samples_to_next_tick <= samples) {
		processed_frames += samples_to_next_tick;
		--m_next_event->note.tick_offset;
		++m_ticks_into_bar; 
		if (m_ticks_into_bar == m_ticks_per_bar) {
			// set up first events for sequences that are starting to play on the 
			// next tick (beginning of next bar)
			for (int id{0}; id < MAX_SEQUENCES; ++id) {
				if (m_start_playing[id]) {
					m_sequences[id]->Reset();
					m_sequences[id]->SetPlaying(true);
					m_next_events[id].sequence_id = id;
					m_next_events[id].note = m_sequences[id]->GetNextEvent();
					m_next_events[id].next = nullptr;
					InsertEventIntoQueue(m_next_events[id]);
					m_start_playing[id] = false;
				}
			}
			m_ticks_into_bar = 0;
		}
		// can have several events in a row with no offset from the previous
		while (m_next_event->note.tick_offset == 0) {
			if (m_next_event->note.play)
				m_write_midi_callback(m_next_event->note.data.data(),
					m_next_event->note.data.size(),
					processed_frames - 1);
			// make the next event the head of the queue
			MidiEventQueueItem* last_event{m_next_event};
			m_next_event = last_event->next;
			// replace the event that was just processed with the next event from 
			// that sequence and put it back in the queue
			if (m_sequences[last_event->sequence_id]->IsPlaying())
			{
				last_event->next = nullptr;
				last_event->note = m_sequences[last_event->sequence_id]->GetNextEvent();
				InsertEventIntoQueue(*last_event);
			}
		}
		m_clock.Advance(samples_to_next_tick);
		samples_to_next_tick = m_clock.SamplesToNextTick();
	}
	m_clock.Advance(samples - processed_frames);
}

void Sequencer::SendAllNoteOffs()
{
	m_send_note_offs = true;
}

void Sequencer::SetWriteMidiCallback(
		std::function<void(midi_data_t* data, size_t size, n_frames_t offset)> write_midi)
{
	m_write_midi_callback = write_midi;
}

size_t Sequencer::AddSequence(std::unique_ptr<StepSequence> &&sequence)
{
	// find the first unnused place in the array
	size_t id{0};
	while (id < m_sequences.size() && m_sequences[id])
		++id;
	if (id == m_sequences.size())
		throw std::out_of_range("Can't add any more sequences");

	m_sequences[id] = std::move(sequence);	
	return id;
}

void Sequencer::RemoveSequence(size_t id)
{
	m_sequences.at(id) = nullptr;
}

StepSequence& Sequencer::GetSequence(size_t id)
{
	if (m_sequences.at(id))
		return *(m_sequences[id]);
	else
		throw std::out_of_range("Index is not valid");
}

void Sequencer::Play()
{
	switch(m_state) {
		case SequencerState::PLAYING:
			break;
		case SequencerState::STOPPED:
			PopulateNextEvents();
		case SequencerState::PAUSED:
			m_state = SequencerState::PLAYING;
			break;
	}
}

void Sequencer::Pause()
{
	switch(m_state) {
		case SequencerState::PLAYING:
			SendAllNoteOffs();
			m_state = SequencerState::PAUSED;
			break;
		default:
			break;
	}
}

void Sequencer::Stop()
{
	switch(m_state) {
		case SequencerState::PLAYING:
		case SequencerState::PAUSED:
			for (auto& sequence : m_sequences)
				if (sequence) sequence->Reset();
			// set any sequences that were due to start playing on the next
			// bar to playing
			for (int id{0}; id < MAX_SEQUENCES; ++id) {
				if (m_start_playing[id])
					m_sequences[id]->SetPlaying(true);
			}
			SendAllNoteOffs();
			m_state = SequencerState::STOPPED;
			break;
		default:
			break;
	}	
}

void Sequencer::SetSequencePlaying(const size_t id, const bool should_play)
{
	if (m_sequences[id]) {
		if (!m_sequences[id]->IsPlaying() && should_play) {
			if (m_state != SequencerState::STOPPED)
				// set in list whether it should start playing at the beginning
				// of the next bar
				m_start_playing[id] = true;
			else
				// if stopped can set it straight away
				m_sequences[id]->SetPlaying(true);
		}
		else if (m_sequences[id]->IsPlaying() && !should_play) {
			m_sequences[id]->SetPlaying(false);
		}
	}
}

bool operator>(const Sequencer::MidiEventQueueItem &first, 
		const Sequencer::MidiEventQueueItem &second)
{
	return first.note.tick_offset > second.note.tick_offset;
}

// Construct the next_events queue from the first event of each active sequence
void Sequencer::PopulateNextEvents()
{
	m_next_event = &m_end_event;
	for (size_t id{0}; id < MAX_SEQUENCES; ++id) {
		if (m_sequences[id] && m_sequences[id]->IsPlaying()) {
			m_next_events[id].note = m_sequences[id]->GetNextEvent();
			m_next_events[id].sequence_id = id;
			m_next_events[id].next = nullptr;
			InsertEventIntoQueue(m_next_events[id]);
		}
	}
}

// Insert an event which has a tick offset relative to the same point as m_next_event (i.e.
// the head of the queue) into the queue
void Sequencer::InsertEventIntoQueue(MidiEventQueueItem& event)
{
	if (event.next)
		throw std::invalid_argument("Trying to insert an event which is linked to another");

	// when we start the offset is from the same point as the next event. Loop until
	// the offset is less than that to the next event decreasing by the next events offset
	// each time since the offsets are between events.
	event.next = m_next_event;
	MidiEventQueueItem* insert_at{nullptr};
	while (event.next != &m_end_event && 
		   event.note.tick_offset >= event.next->note.tick_offset) {
		event.note.tick_offset -= event.next->note.tick_offset;
		insert_at = event.next;
		event.next = event.next->next;
	}
	if (insert_at)
		// event is next event
		insert_at->next = &event;
	else {
		event.next = m_next_event;
		m_next_event = &event;
	}
	// works even if next is m_end_event and we don't care what it's offset is
	event.next->note.tick_offset -= event.note.tick_offset;
}
