#pragma once

#include <utility>
#include "types.h"

/* Converts from samples to beats. Given a tempo, number of ticks per beat and sample
 * rate, the clock calculates the number of samples until the next tick. It adjusts i
 * for rounding errors which occur when a beat is not an integer number of samples. 
 * By default the number of ticks per beat will be 24. This allows for semidemiquavers 
 * and demisemiquaver triplets.
 */
class Clock
{
public:
	Clock(const double bpm, const n_frames_t sample_rate, const int ticks_per_beat = 24);
	Clock() = default;

	// Initially will return 0. Once the clock is advance, it will return the number of
	// samples until the next tick.
	inline n_frames_t SamplesToNextTick();
	// Advances the clocks position
	void Advance(n_frames_t frames);

	inline void UpdateTiming(const int steps_per_beat, const double bpm, 
			const n_frames_t sample_rate);
private:
	n_frames_t m_frames_to_next_tick;
	n_frames_t m_frames_per_tick;
	// the difference between the frames per tick that is used and the rational
	// value that is calculated from tempo, ticks per beat and sample rate. 
	// first is numerator, second is denominator.
	std::pair<int, int> m_fraction_per_tick;
	// numerator of the running offset which is used in calculations
	n_frames_t m_offset_numerator;
};

inline n_frames_t Clock::SamplesToNextTick()
{
	return m_frames_to_next_tick;
}

inline void Clock::UpdateTiming(const int steps_per_beat, const double bpm,
		const n_frames_t sample_rate)
{
	// convert bpm to and in representing beats per 10000 seconds  so that 
	// we can keep exact time to a tempo with two decimal places.
	auto bpMs = static_cast<int>((bpm / 60) * 10000);
	m_frames_per_tick = (sample_rate * 10000) / (bpMs * steps_per_beat);
	m_fraction_per_tick.first = (sample_rate * 10000) % (bpMs * steps_per_beat);
	m_fraction_per_tick.second = (bpMs * steps_per_beat);
}
