#include "command_queue.h"

#include <chrono>
#include <thread>
#include <optional>

void CommandQueue::Push(std::unique_ptr<Command>&& command)
{
	while (m_queue.count() == COMMAND_QUEUE_SIZE)
		std::this_thread::sleep_for(m_wait_duration);
	m_queue.push(std::move(command));
}

std::optional<Update> CommandQueue::DoNextCommand(Sequencer& sequencer)
{
	if (m_queue.empty())
		return {};
	auto opt_update = m_queue.front()->execute(sequencer);
	m_queue.pop();
	return opt_update;
}
	
PlaybackCommand::PlaybackCommand(PlaybackArg arg) : m_arg{arg}
{
}

std::optional<Update> PlaybackCommand::execute(Sequencer& sequencer) const
{
	switch(m_arg) {
		case PLAY:
			sequencer.Play();
			break;
		case PAUSE:
			sequencer.Pause();
			break;
		case STOP:
			sequencer.Stop();
			break;
	}
	return {};
}

SetSequencePlayingCommand::SetSequencePlayingCommand(const int sequence_id, 
		const bool playing) : m_sequence_id{sequence_id}, m_playing{playing}
{
}

std::optional<Update> SetSequencePlayingCommand::execute(Sequencer& sequencer) const
{
	sequencer.SetSequencePlaying(m_sequence_id, m_playing);
	return {};
}

SetStepParameterCommand::SetStepParameterCommand(
		int sequence_id, int step, StepParam param, char value)
	: m_sequence_id{sequence_id}
	, m_step{step}
	, m_param{param}
	, m_value{value}
{
}

std::optional<Update> SetStepParameterCommand::execute(Sequencer& sequencer) const
{
	switch (m_param)
	{
	case NOTE:
		sequencer.GetSequence(m_sequence_id).SetNote(m_step, m_value);
		break;
	case VELOCITY:
		sequencer.GetSequence(m_sequence_id).SetVelocity(m_step, m_value);
		break;
	case PLAY:
		sequencer.GetSequence(m_sequence_id).SetPlayNote(m_step, m_value);
		break;
	};	
	return {};
}
