#pragma once 
#include <cstddef>
#include <vector>
#include <array>

using midi_data_t = unsigned char;

struct Step
{
	midi_data_t note = 64;
	midi_data_t velocity = 64;;
	bool play = true;
};

struct  MidiNote
{
	std::array<midi_data_t, 3> data; 
	int tick_offset;
	bool play = true;
};

class StepSequence
{
public:
	MidiNote GetNextEvent();

	void SetNote(const size_t step, const midi_data_t note);
	void SetVelocity(const size_t step, const midi_data_t velocity);
	void SetPlayNote(const size_t step, const bool play);

	bool IsPlaying();
	void SetPlaying(const bool playing);
	void Reset();

	StepSequence(const size_t steps, const int ticks_per_step,
			const int note_duration);
	StepSequence(
			const int ticks_per_step,
			const int note_duration,
			const std::vector<midi_data_t> &notes,
			const std::vector<midi_data_t> &velocities);

	StepSequence() = default;

	static const size_t BEAT;
	static const size_t HALF_BEAT;
	static const size_t QUARTER_BEAT;
	static const size_t TRIPLET;
	static const size_t HALF_BEAT_TRIPLET;
	static const size_t QUARTER_BEAT_TRIPLET;
private:
	std::vector<Step> m_events;
	size_t m_position;
	int m_ticks_per_step;
	int m_note_duration;
	int m_next_offset;
	bool m_is_playing;
	bool m_note_on;
};


