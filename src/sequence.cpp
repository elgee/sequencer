#include "sequence.h"
#include <vector>
#include <stdexcept>

const size_t StepSequence::BEAT{24};
const size_t StepSequence::HALF_BEAT{12};
const size_t StepSequence::QUARTER_BEAT{6};
const size_t StepSequence::TRIPLET{8};
const size_t StepSequence::HALF_BEAT_TRIPLET{4};
const size_t StepSequence::QUARTER_BEAT_TRIPLET{2};

StepSequence::StepSequence(
			const int ticks_per_step,
			const int note_duration,
			const std::vector<midi_data_t> &notes,
			const std::vector<midi_data_t> &velocities)
	: m_events(notes.size())
	, m_position{0}
	, m_ticks_per_step{ticks_per_step}
	, m_note_duration{note_duration}
	, m_next_offset{1}
	, m_is_playing{false}
	, m_note_on{false}
{
	if (notes.size() != velocities.size())
		throw std::invalid_argument("Size of notes and velocities must be the same");
	if (ticks_per_step < 0)
		throw std::invalid_argument("ticks_per_step must be positive");
	if (note_duration < 0 || note_duration > ticks_per_step)
		throw std::invalid_argument("note_duration must be positive and less than the"
				"number of ticks per step");

	for (size_t i{0}; i < m_events.size(); ++i) {
		m_events[i].note = notes[i];
		m_events[i].velocity = velocities[i];
		m_events[i].play = true;
	}
}

StepSequence::StepSequence(const size_t steps, const int ticks_per_step,
		const int note_duration)
	: m_events(steps)
	, m_position{0}
	, m_ticks_per_step{ticks_per_step}
	, m_note_duration{note_duration}
	, m_next_offset{1}
	, m_is_playing{false}
	, m_note_on{false}
{
}

MidiNote StepSequence::GetNextEvent()
{
	MidiNote return_note{
			{0x80, m_events[m_position].note, m_events[m_position].velocity},
			m_next_offset,
			m_events[m_position].play
	};
	if (m_note_on) {
		// make sure event is set to play in case it has been changed since the
		// note started
		return_note.play = true;
		// update for next call
		m_note_on = false;
		m_next_offset = m_ticks_per_step - m_note_duration;
		m_position = ++m_position % m_events.size();
	}
	else {
		// note on message
		return_note.data[0] = 0x90;
		// next call depends on whether this step is being played or not
		if (return_note.play) {
			// don't increment position since we need to send a note off message next time
			m_next_offset = m_note_duration;
			m_note_on = true;
		}
		else {
			m_next_offset = m_ticks_per_step;
			m_position = ++m_position % m_events.size();
		}
	}
	return return_note;
}

void StepSequence::SetNote(const size_t step, const midi_data_t note)
{
	m_events[step].note = note;
}

void StepSequence::SetVelocity(const size_t step, const midi_data_t velocity)
{
	m_events[step].velocity = velocity;
}

void StepSequence::SetPlayNote(const size_t step, const bool play)
{
	m_events[step].play = play;
}

bool StepSequence::IsPlaying()
{
	return m_is_playing || m_note_on;
}

void StepSequence::SetPlaying(const bool playing)
{
	m_is_playing = playing;
}

// Resets the sequence. Only use it while the sequence is stopped
void StepSequence::Reset()
{
	m_position = 0;
	m_next_offset = 1;
	m_note_on = false;
}
