#pragma once

#include <cstdint>
#include <vector>

#include <jack/jack.h>

using std::size_t;

using n_frames_t = jack_nframes_t;
using midi_data_t = unsigned char;

struct MidiEvent
{
	n_frames_t time;
	std::vector<midi_data_t> data;
};
