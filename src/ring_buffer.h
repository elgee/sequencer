#include <cstddef>
#include <array>
#include <assert.h>
#include <atomic>
#include <stdexcept>
#include <utility>

template <class T, size_t N>
class RingBuffer final
{
public:
	RingBuffer() 
		: m_data{}, m_push_count{0}, m_pop_count{0}, m_head{0}, m_tail{0} {}
	
	void pop()
	{
		// don't pop when empty
		assert(!empty());
		m_tail = ++m_tail % N;
		m_pop_count.fetch_add(1, std::memory_order_release);
	}

	T& front()
	{
		return m_data[m_tail];
	}

	void push(const T& item)
	{
		if (count() == N)
			throw std::out_of_range("Attempted to add to a full ringbuffer");
		m_data[m_head] = item;
		m_head = ++m_head % N;
		m_push_count.fetch_add(1, std::memory_order_release);
	}

	void push(T&& item)
	{
		if (count() == N)
			throw std::out_of_range("Attempted to add to a full ringbuffer");
		m_data[m_head] = std::move(item);
		m_head = ++m_head % N;
		m_push_count.fetch_add(1, std::memory_order_release);
	}
	
	bool empty() { return m_head == m_tail; }

	size_t count()
	{
		return m_push_count.load(std::memory_order_acquire) - 
			m_pop_count.load(std::memory_order_acquire);
	}

private:
	std::array<T, N> m_data;
	// there are two sets of indices; atomic ones which insert the appropriate memory fences
	// and can overflow and the others which are used to index into data.
	std::atomic_uint m_push_count;
	std::atomic_uint m_pop_count;
	std::size_t m_head;
	std::size_t m_tail;
};
