#pragma once

#include "sequence.h"
#include "clock.h"
#include "types.h"

#include <memory>
#include <functional>

enum class SequencerState
{
	PLAYING,
	PAUSED,
	STOPPED
};

class Sequencer
{
public:
	void ProcessNFrames(const n_frames_t);
	void SetWriteMidiCallback(std::function<void(midi_data_t* data, size_t size,
				n_frames_t offset)>);

	// sequences are moved to avoid allocating memory on the audio thread
	size_t AddSequence(std::unique_ptr<StepSequence> &&sequence);
	void RemoveSequence(size_t id);
	StepSequence& GetSequence(size_t id);

	// These control the seqeuencer's state. They do not alter whether each individual 
	// sequence is playing as that is to track which sequences should play when the 
	// sequencer _is_ running. However, stopping does reset all the sequence positions.
	void Play();
	void Pause();
	void Stop();

	// Sets a sequence's playing state. This doesn't happen immediately unless the 
	// sequencer is stopped. When playing/paused sequences will start on the first beat
	// of the next bar and stop once it finishes its current loop
	void SetSequencePlaying(const size_t id, const bool should_play);

	Sequencer(const double bpm, const int beats_per_bar, const n_frames_t sample_rate);
	Sequencer() = delete;
	
	struct MidiEventQueueItem
	{
		size_t sequence_id = Sequencer::MAX_SEQUENCES;
		MidiNote note;
		MidiEventQueueItem* next = nullptr;
	};
	friend bool operator>(const MidiEventQueueItem &first, const MidiEventQueueItem &second);
protected:
	void PopulateNextEvents();
	void InsertEventIntoQueue(MidiEventQueueItem& event);
	void UpdateAndWriteMidiEvents(const n_frames_t samples);
	void SendAllNoteOffs();

	static const size_t MAX_SEQUENCES = 32;
	static const int ticks_per_beat = 24;

	SequencerState m_state;

	std::array<std::unique_ptr<StepSequence>, MAX_SEQUENCES> m_sequences;
	int m_active_sequences;

	Clock m_clock;
	int m_ticks_into_bar;
	int m_ticks_per_bar;

	std::function<void(midi_data_t* data, size_t size, n_frames_t offset)> 
	m_write_midi_callback;

	// start playing sequence on next bar
	std::array<bool, MAX_SEQUENCES> m_start_playing;
	bool m_send_note_offs;

	// the next event for every sequence. Each event contains a pointer to the event 
	// which will occur after it or a nullptr if it is the last event. The tick offset
	// of each event is the offset from the previous event.
	std::array<MidiEventQueueItem, MAX_SEQUENCES> m_next_events;
	// the next event to be played
	MidiEventQueueItem m_end_event;
	MidiEventQueueItem* m_next_event;

};



