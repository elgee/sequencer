#include "clock.h"

#include "types.h"
#include <vector>

Clock::Clock(const double bpm, const n_frames_t sample_rate, const int steps_per_beat)
{
	UpdateTiming(steps_per_beat, bpm, sample_rate);
	m_frames_to_next_tick = m_frames_per_tick;
	m_offset_numerator = m_fraction_per_tick.first;
}

void Clock::Advance(n_frames_t n_frames)
{
	auto frames_left = n_frames;
	while (m_frames_to_next_tick <= frames_left) {
		frames_left -= m_frames_to_next_tick;
		m_offset_numerator += m_fraction_per_tick.first;
		m_frames_to_next_tick = m_frames_per_tick + 
			m_offset_numerator / m_fraction_per_tick.second;
		m_offset_numerator %= m_fraction_per_tick.second;
	}
	m_frames_to_next_tick -= frames_left;
}

